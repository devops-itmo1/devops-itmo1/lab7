FROM node:18-alpine as builder

WORKDIR /react

# copy the package.json to install dependencies
COPY package.json ./

# Install the dependencies and make the folder
RUN yarn

COPY . ./

# Build the project and copy the files
RUN yarn build

FROM nginx:1.12-alpine

COPY --from=builder /react/dist /usr/share/nginx/html

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]